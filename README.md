## README ##

### About JLSystem ###
JLSystem is an engine to generate and visualize Lindenmayer systems (L-System) written in Java. 
### Import JLSystem into Eclipse ###
1. **Import the project** as a Maven project into your eclipse.

	*File -> Import... -> Maven -> Existing Maven Projects*

2. Configure the Build Path and **add EtherGL**
	
	You have to pull the EtherGL project repository ([https://bitbucket.org/arisona/ether-gl](https://bitbucket.org/arisona/ether-gl)) and import it into Eclipse as a normal Java project. Finally you need to add the EtherGL project to the JLSystem project:	

	*Choose the jlsystem in your Eclipse. Project -> Properties -> Java Build Path -> Projects -> Add...* And select the EtherGL project. 

3. **Add libraries** to the Build Path & choose the **correct java version**
	
	Let's do maven its job! ;-)
	If you want to change the java version, change the *java.version* property in the pom.xml file and update your project (*Maven->Update project*... or *ALT-F5*)