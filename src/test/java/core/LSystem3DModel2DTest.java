package core;

import javafx.scene.control.PopupControl;
import core.command.DrawForwardCommand;
import core.command.ICommand;
import core.command.PopCommand;
import core.command.PushCommand;
import core.command.RollCommand;
import core.ether.LSystemWindow;
import core.model.ILSystem3DModel;
import core.model.LSystem3DModel;
import core.model.LSystem3DModel3DCubic;

/**
 * 
 * @author Lukas Keller
 * @version 1.0
 */
public class LSystem3DModel2DTest 
{

	private LSystemWindow window;
	
	public static void main(String[] args)
	{
		new LSystem3DModel2DTest();
	}
	
	public LSystem3DModel2DTest() 
	{
		this.setUp();
		this.simpleTest();
	}
	
	public void setUp()
	{
		this.window = new LSystemWindow("TEST");
	}
	
	private void simpleTest()
	{
		ILSystem3DModel planeModel = new LSystem3DModel3DCubic();//new LSystem3DModel2D(); //new  new LSystem3DModel3DCubic();
		ILSystem3DModel lineModel = new LSystem3DModel();
		
		ICommand forward = new DrawForwardCommand();
		ICommand push = new PushCommand();
		ICommand pop = new PopCommand();
		
		push.exec(planeModel);
		
		forward.exec(planeModel);
//		forward.exec(lineModel);
		
		RollCommand rotate = new RollCommand();
		rotate.setAngle(63);
		
		
		
		
//		rotate.exec(planeModel);
//		rotate.exec(lineModel);
		
//		forward.exec(planeModel);
//		forward.exec(lineModel);
		
		pop.exec(planeModel);
		this.window.addModel(planeModel);
//		this.window.addModel(lineModel);
	}
}