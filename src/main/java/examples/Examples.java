package examples;

import org.apache.logging.log4j.LogManager;

import core.Driver;

import org.apache.logging.log4j.Logger;

/**
 * @author Lukas Keller
 * @version 1.2
 */
public class Examples 
{
	private static IRun currentExample = Example.PLANT;
	
	private static Logger logger = LogManager.getLogger(Examples.class);
	
	private enum Example implements IRun
	{
		PLANT("plant.json", "Plant", 5,5),
		SIERPINSKI_TRIANGLE("SierpinskiTriangle.json","SierpinskiTriangle",8,8),
		SIMPLE_TREE("SimpleTree.json","SimpleTree",6,6),
		KOCH_CURVE("KochCurve.json","KochCuve",6,6),
		STEM_3D_SIMPLE_2("Stem3D_2.json","Stem3DSimple2",5,5),
		STEM_3D_SIMPLE_3("Stem3D_3.json","Stem3DSimple3",6,6),
		HILBERT_3D("Hilbert3D.json","Hilbert3D",3,3),
		HILBERT_3D_2("Hilbert3D_2.json","Hilbert3D_2",4,4);
		
		private final String PATH ="src/main/resources/examples/";
		private final String fileName;
		private final String name;
		private final int maxEvolutions;
		private final int defaultShownEvolution;
		
		Example(String fileName, String name, int maxEvolutions, int defaultShownEvolution)
		{
			this.fileName = this.PATH + fileName;
			this.name = name;
			this.maxEvolutions = maxEvolutions;
			this.defaultShownEvolution = defaultShownEvolution;
		}
		
		public void run()
		{
			try 
			{
				Driver.run(this.fileName, this.name, this.maxEvolutions, this.defaultShownEvolution);
			}
			catch (Exception e)
			{
				logger.error(e.getMessage(),e);
				System.exit(-1);
			}
		}
	}
	
	private enum SimpleExample implements IRun
	{
		STRING("F1+1F1-1F1-1F1+1F1");
		
		private final String commands;
		
		private SimpleExample(String commands) 
		{
			this.commands = commands;
		}
		
		@Override
		public void run()
		{
			try 
			{
				Driver.run(this.commands,this.name());
			}
			catch (Exception e)
			{
				logger.error(e.getMessage(),e);
				System.exit(-1);
			}
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		if(args.length == 0)	//no input, run the example
		{
			Examples.logger.info("Input is empty ...");
			Examples.logger.info("run default example...");
			Examples.currentExample.run();
		}
		else if(args.length==1)	//input is a name of an example or a command string (must start with cmd:)
		{
			Examples.logger.info("Input has lenght of 1...");
			String input = args[0];
			
			if(input.startsWith("cmd:"))	//input is a command string (for example: "F+F-F") (with basic commands)
			{
				String commands = input.substring(4); //after "cmd:"
				Examples.logger.info("Input is own string of commands (" + commands + ")...");
				
				try 
				{
					Driver.run(commands, "Own command string");
				} 
				catch (Exception e) 
				{
					logger.error(e.getMessage(),e);
					System.exit(-1);
				}
			}
			else
			{
				Examples.logger.info("Input is a name of an example...");
				try
				{
					Example.valueOf(input).run(); //must be for example "KOCH_CURVE" 
				}
				catch(IllegalArgumentException e1)
				{
					try
					{
						SimpleExample.valueOf(input).run();
					}
					catch(IllegalArgumentException e2)
					{
						throw new IllegalArgumentException("Could not find a example or simple example with the name " + input);
					}
				}
			}
		}
		else if(args.length==4) //input is a specific example (or another new example)
		{
			Examples.logger.info("Input an external file...");
			
			try
			{
				Driver.run(args[0], args[1],Integer.parseInt(args[2]),Integer.parseInt(args[3]));
			}
			catch (Exception e) 
			{
				logger.error(e.getMessage(),e);
				System.exit(-1);
			}
		}
		else
		{
			logger.error(new IllegalArgumentException("The input could not be matched. Your input was " + args.length +" long."));
		}
	}
	
	/**
	 * @author Lukas Keller
	 * @version 1.0
	 */
	private interface IRun
	{
		public void run();
	}
}