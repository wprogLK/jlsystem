package core.evolution;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.LSystem;

/**
 * @author Lukas Keller
 * @version 1.0
 */
public class EvolutionIterator
{
	private static Logger logger = LogManager.getLogger(EvolutionIterator.class);
	
	private final LSystem SYSTEM;
	
	private int currentEvolution;
	private final int MAX_EVOLUTION;
	
	public EvolutionIterator(LSystem system, int startEvolution, int maxEvolution) 
	{
		this.SYSTEM = system;
		this.MAX_EVOLUTION = maxEvolution;
		this.currentEvolution = startEvolution;
	}
	
	public Evolution nextEvolution()
	{
		this.setCurrentEvolution(this.currentEvolution+1);
		return this.currentEvolution();
	}
	
	public Evolution prevEvolution()
	{
		this.setCurrentEvolution(this.currentEvolution-1);
		return this.currentEvolution();
	}
	
	public Evolution currentEvolution()
	{
		return this.SYSTEM.getEvolution(this.currentEvolution);
	}
	
	public void setCurrentEvolution(int currentEvolution)
	{
		int ev = Math.min(currentEvolution, this.MAX_EVOLUTION);
		ev = Math.max(ev, 0);
		
		assert(ev<=this.MAX_EVOLUTION && ev>=0);
		
		this.currentEvolution = ev;
	}
}