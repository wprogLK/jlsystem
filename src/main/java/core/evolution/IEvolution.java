package core.evolution;

import core.model.ILSystem3DModel;

/**
 * @author Lukas Keller
 * @version 1.0
 */
public interface IEvolution 
{
	public void exec();
	public ILSystem3DModel getModel();
	public int getEvolutionNumber();
	public String getRawCommands();
}
