package core.evolution;

import java.util.List;

import core.command.ICommand;
import core.interpreters.IInterpreter;
import core.model.ILSystem3DModel;
import core.model.LSystem3DModel3DCircle;
import core.model.LSystem3DModel3DCubic;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * An evolution contains its raw commands as a list of {@link ICommand}s.
 * @author Lukas Keller
 * @version 1.4
 */
public class Evolution implements IEvolution
{
	private static Logger logger = LogManager.getLogger(Evolution.class);
	
	private final String rawCommands;
	private final List<ICommand> commands;
	private final int evolutionNumber;
	
	private ILSystem3DModel model;
	
	private String name;
	
	/**
	 * 
	 * @param commands list of raw commands
	 * @param evolutionNumber the number of evolution
	 */
	public Evolution(String rawCommands, List<ICommand> commands, int evolutionNumber, String name) 
	{
		this.rawCommands = rawCommands;
		this.commands = commands;
		this.evolutionNumber = evolutionNumber;
		this.name = name;
		
//		this.model = new LSystem3DModel(); 
//		this.model = new LSystem3DModel2D();
//		this.model = new LSystem3DModel3DCubic();
		this.model = new LSystem3DModel3DCircle();
		this.exec();
	}
	
	/**
	 * 
	 * @param commands list of raw commands
	 * @param evolutionNumber the number of evolution
	 */
	public Evolution(String rawCommands, List<ICommand> commands, boolean isPlant, int evolutionNumber, String name) 
	{
		this.rawCommands = rawCommands;
		this.commands = commands;
		this.evolutionNumber = evolutionNumber;
		this.name = name;
		
//		this.model = new LSystem3DModel(isPlant); 
//		this.model = new LSystem3DModel2D(isPlant);
//		this.model = new LSystem3DModel3DCubic(isPlant);
		this.model = new LSystem3DModel3DCircle(isPlant);
		
		this.exec();
	}
	
	/**
	 * this is only for testing!
	 * @param rawCommands
	 * @param interpreter
	 * @throws Exception 
	 */
	public Evolution(String rawCommands, IInterpreter interpreter) throws Exception
	{
		this(rawCommands, interpreter.interpret(rawCommands),0, interpreter.getName());
	}
	
	/**
	 * Executes all commands.
	 */
	public void exec()
	{
		for(ICommand cmd: this.commands)
		{
			cmd.exec(this.model);
		}
		//name ; evolution number ; #vertices ; stringCommandLenght ; #ICommands
		logger.info(this.name + ";" + this.getEvolutionNumber() +";" + this.model.getNumberOfVertices() +";" + this.rawCommands.length() + ";" + this.commands.size());
		
//		logger.info("\n*STAT - "+ this.name +" * :\nEvolution: " + this.evolutionNumber +"\n\t*Number of vertices after exec: " + this.model.getNumberOfVertices() +"\n\t*Length of string: " + this.rawCommands.length() +"\n\t*Size of commands: " + this.commands.size());
	}

	public ILSystem3DModel getModel()
	{
		return this.model;
	}
	
	public int getEvolutionNumber()
	{
		return this.evolutionNumber;
	}
	
	public String getRawCommands()
	{
		return this.rawCommands;
	}
}
