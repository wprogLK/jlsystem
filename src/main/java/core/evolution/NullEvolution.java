package core.evolution;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.model.ILSystem3DModel;
import core.model.LSystem3DModel;

/**
 * An empty evolution
 * @author Lukas Keller
 * @version 1.1
 */
public class NullEvolution implements IEvolution
{
	private static Logger logger = LogManager.getLogger(NullEvolution.class);
	
	public NullEvolution() 
	{
	}

	@Override
	public void exec()
	{
		//Do nothing or drink tea
	}

	@Override
	public ILSystem3DModel getModel() 
	{
		return new LSystem3DModel();
	}

	@Override
	public int getEvolutionNumber() 
	{
		return 0;
	}

	@Override
	public String getRawCommands()
	{
		return "";
	}
}
