package core.coders;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

/**
 * 
 * @author Lukas Keller
 * @version 1.5
 */
public class StringCoder extends AbstractCoder<String>
{
	private static Logger logger = LogManager.getLogger(StringCoder.class);

	public StringCoder(JSONObject rules, JSONObject commands)
	{
		super(rules, commands);
	}

	public String encode(String input) throws Exception 
	{
		String encodedInput = "";

		int commandCounter = 1;

		String command = this.getNextCommand(input, 0);

		for (int i = command.length(); i < input.length(); i += command.length()) 
		{
			String nextCommand = this.getNextCommand(input, i);

			if (nextCommand.equals(command))
			{
				commandCounter++;
			} 
			else
			{
				encodedInput += this.combine(command, commandCounter);
				command = nextCommand;
				commandCounter = 1;
			}
		}

		encodedInput += this.combine(command, commandCounter);

		return encodedInput;
	}

	private String combine(String command, int commandCounter)
	{
		return command + commandCounter;
	}

	public String decode(String input)
	{
		//TODO
		
		throw new UnsupportedOperationException();
	}
}