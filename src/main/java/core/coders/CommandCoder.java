package core.coders;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import core.command.ICommand;

/**
 * 
 * @author Lukas Keller
 * @version 1.0
 */
public class CommandCoder extends AbstractCoder<List<ICommand>>
{
	private static Logger logger = LogManager.getLogger(CommandCoder.class);

	public CommandCoder(JSONObject rules, JSONObject commands)
	{
		super(rules,commands);
	}

	public List<ICommand> encode(List<ICommand> input) throws Exception 
	{
		List<ICommand> encodedCommands = new ArrayList<ICommand>();
		
		throw new UnsupportedOperationException();
	}

	public List<ICommand> decode(List<ICommand> input)
	{
		//TODO
		
		throw new UnsupportedOperationException();
	}
}