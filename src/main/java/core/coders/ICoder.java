/**
 * 
 */
package core.coders;

/**
 * @author Lukas Keller
 * @version 1.0
 */
public interface ICoder<T> 
{
	public T encode(T input) throws Exception ;
	public T decode(T input);
	public boolean existsRuleStartingWith(String prefix);
	public boolean existsRule(String rule);
	public boolean existsCommandStartingWith(String prefix);
	public boolean existsCommand(String command);
}
