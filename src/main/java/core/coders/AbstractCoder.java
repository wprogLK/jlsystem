package core.coders;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

/**
 * 
 * @author Lukas Keller
 * @version 1.0
 */
public abstract class AbstractCoder<T> implements ICoder<T>
{
	protected final JSONObject RULES;
	protected final JSONObject COMMANDS;
	
	private static Logger logger = LogManager.getLogger(AbstractCoder.class);

	public AbstractCoder(JSONObject rules, JSONObject commands)
	{
		this.RULES = rules;
		this.COMMANDS = commands;
	}

	public abstract T encode(T input) throws Exception;

	public abstract T decode(T input);

	public final String getNextCommand(String input, int start) throws Exception 
	{
		String command = "";

		for (int i = start; i < input.length(); i++)
		{
			char currentChar = input.charAt(i);

			if (this.existsCommandStartingWith(command)) 
			{
				if (this.existsCommand(command))
				{
					//a command was found ...
					return command;
				} 
				else
				{
					//command is not complete yet ...
					command += currentChar;
				}
			}
		}

		if (this.existsCommandStartingWith(command)) 
		{
			if (this.existsCommand(command)) 
			{
				//a command was found ...
				return command;
			} 
			else 
			{
				//command is not complete yet ...
			}
		}

		throw new Exception("ERROR: Command " + command + " does not exist!"); //should never happen... TODO: throw better an exception (e.g. LSystemException)
	}

	public final boolean existsRuleStartingWith(String prefix)
	{
		for (Object keyObject : this.RULES.keySet())
		{
			String key = (String) keyObject;

			if (key.startsWith(prefix))
			{
				return true;
			}
		}

		return false;
	}

	public final boolean existsRule(String rule)
	{
		return this.RULES.containsKey(rule);
	}

	public final boolean existsCommandStartingWith(String prefix)
	{
		for (Object keyObject : this.COMMANDS.keySet()) 
		{
			String key = (String) keyObject;

			if (key.startsWith(prefix))
			{
				return true;
			}
		}

		return false;
	}

	public final boolean existsCommand(String command)
	{
		return this.COMMANDS.containsKey(command);
	}
}