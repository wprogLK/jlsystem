package core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.ether.LSystemWindow;
import core.evolution.Evolution;
import core.evolution.EvolutionIterator;
import core.interpreters.CodingInterpreter;

/**
 * @author Lukas Keller
 * @version 1.5
 */
public class Driver
{
	private static Logger logger = LogManager.getLogger(Driver.class);
	
	/**
	 * @throws Exception 
	 * 
	 */
	public static void run(String filePath, String name, int maxEvolutions, int evolutionToShow) throws Exception
	{
		assert(maxEvolutions>=evolutionToShow);
		
		LSystem system = LSystem.createCommandCodingSystem(filePath, name, maxEvolutions);
//		LSystem system = LSystem.createCodingSystem(filePath, name, maxEvolutions);
//		LSystem system = LSystem.createSimpleSystem(filePath, name, maxEvolutions);
		
		EvolutionIterator evolutionIterator = new EvolutionIterator(system, evolutionToShow, maxEvolutions);
		
		LSystemWindow window = new LSystemWindow(name, evolutionIterator);
		
		try
		{
			window.setEvolution(system.getEvolution(evolutionToShow));
		}
		catch(Exception e)
		{
			logger.error(e.getMessage(), e);
			System.exit(1);
		}
	}
	
	public static void run(String commands, String name) throws Exception
	{
		LSystemWindow window = new LSystemWindow(name);
		
		CodingInterpreter interpreter = new CodingInterpreter();
		
		if(!interpreter.isEncoded(commands))
		{
			throw new Exception("Input must be encoded!");
		}
		
		try
		{
			window.setEvolution(new Evolution(commands, interpreter));
		}
		catch(Exception e)
		{
			logger.error(e.getMessage(), e);
			System.exit(1);
		}
	}
}
