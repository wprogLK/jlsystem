package core.interpreters;

import java.util.List;

import core.command.ICommand;
import core.evolution.Evolution;

/**
 * @author Lukas Keller
 * @version 1.1
 *
 */
public interface IInterpreter<T>
{
	public List<ICommand> interpret(T input) throws Exception;
	public Evolution[] createEvolutions(T[] rawEvolutions) throws Exception;
	public String getName();
}