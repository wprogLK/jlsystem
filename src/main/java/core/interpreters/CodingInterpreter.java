package core.interpreters;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.command.ICommand;
import core.evolution.Evolution;

/**
 * @author Lukas Keller
 * @author Kyriakos Schwarz 
 * @version 2.4
 */
public class CodingInterpreter extends AbstractInterpreter<String>
{
	private static Logger logger = LogManager.getLogger(CodingInterpreter.class);

	public CodingInterpreter()
	{
		super();
	}
	
	public CodingInterpreter(String filePath)
	{
		super(filePath);
	}
	
	public List<ICommand> interpret(String encodedInput) throws Exception
	{
		/*
		 * invariant: the input string contains ONLY existing commands and is already encoded!!
		 */
	
		assert(this.isEncoded(encodedInput));
		
		List<ICommand> commands = new ArrayList<ICommand>();
		
		int i = 0;
		
		while(i<encodedInput.length())
		{
			String command = this.CODER.getNextCommand(encodedInput, i);
			
			int j=i+command.length();
			String stepString = "";
			char currentChar = encodedInput.charAt(j);
			
			do
			{
				currentChar = encodedInput.charAt(j);
				stepString+=currentChar;
				j++;
				
			}while(j<encodedInput.length() && !this.CODER.existsCommandStartingWith(encodedInput.charAt(j)+""));
			
			int stepNumber = Integer.parseInt(stepString);
			
			commands.add(this.findCommand(command,stepNumber));
			
			i+=command.length()+stepString.length();
		}
	
		return commands;
	}
	
	public Evolution interpret(String input, int evolutionNumber) throws Exception
	{
		List<ICommand> commands = this.interpret(input);
		
		Evolution evolution = new Evolution(input, commands, this.isPlant, evolutionNumber, this.getName()+" - coding");
		
		return evolution;
	}
	
	public Evolution[] createEvolutions(String[] rawEncodedEvolutions) throws Exception
	{
		Evolution[] evolutions = new Evolution[rawEncodedEvolutions.length];
		
		for(int i=0;i<rawEncodedEvolutions.length;i++)
		{
			String rawEncodedEvolution = rawEncodedEvolutions[i];
			evolutions[i] = this.interpret(rawEncodedEvolution,i);
		}
		
		return evolutions;
	}
	
	public final boolean isEncoded(String input)
	{
		return Character.isDigit(input.charAt(input.length()-1));
	}
}