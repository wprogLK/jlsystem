package core.interpreters;

import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import core.coders.StringCoder;
import core.command.ICommand;
import core.evolution.Evolution;

/**
 * @author Lukas Keller
 * @author Kyriakos Schwarz 
 * @version 2.1
 */
public abstract class AbstractInterpreter<T> implements IInterpreter<T>
{
	private static Logger logger = LogManager.getLogger(AbstractInterpreter.class);
	
	private final String FILE_PATH;
	private JSONObject jsonObject;
	private String packageName = "core.command.";
	
	private String name;
	protected JSONObject commands;
	protected JSONObject rules;
	protected boolean isPlant = true;
	
	protected final StringCoder CODER;
	
	private final static String PATH ="src/main/resources/examples/";
	private final static String BASIC_FILE_PATH = "Basic.json";
	
	public AbstractInterpreter()
	{
		this(PATH+BASIC_FILE_PATH);
	}
	
	public AbstractInterpreter(String filePath)
	{
		this.FILE_PATH = filePath;
		this.init();
		
		this.CODER = new StringCoder(this.rules, this.commands);
	}
	
	private void init()
	{
		try
		{
			FileReader reader = new FileReader(this.FILE_PATH);
			
			JSONParser jsonParser = new JSONParser();
			jsonObject = (JSONObject) jsonParser.parse(reader);
			this.commands = (JSONObject) jsonObject.get("commands");
			this.rules = (JSONObject) jsonObject.get("rules");
			this.name = (String) jsonObject.get("name");
			String str = (String) jsonObject.get("isPlant");
			this.isPlant = "true".equals(str);
		} 
		catch (IOException | ParseException e)
		{
			logger.error(e.getMessage(),e);
			System.exit(1);
		}
	}
	
	protected final ICommand findCommand(String symbol, int steps)
	{
		ICommand command = this.findCommand(symbol);
		command.setSteps(steps);
		return command;
	}
	
	protected final ICommand findCommand(String symbol) //TODO: Refactor and comment this method!
	{
		//FIXME: if parameter typ is an int or Integer there is a problem. (Integer.parseInt(String)) NOT Integer.parseInteger(String)!!)
		String delimiter = ";";
		
		try
		{
			JSONObject commands = (JSONObject) jsonObject.get("commands");
			JSONObject command = null;
			try
			{
				command = (JSONObject) commands.get(symbol);
			}
			catch(ClassCastException e)
			{
				//there is just a basic command and not a JSON commands...
				String simpleCommand = (String) commands.get(symbol);
				Object instance = Class.forName(packageName+simpleCommand+"Command").newInstance();
				
				ICommand cmd = (ICommand) instance;
				cmd.setLiteral(symbol);

				return cmd;
			}
			
			String name = (String) command.get("name");
			String arguments = (String) command.get("arguments");	//arguments separate by ";"
			String varNames = (String) command.get("varNames");		//varNames separate by ";"
			
			Object instance = Class.forName(packageName+name+"Command").newInstance();
			
			String[] singleArguments = arguments.split(delimiter);
			String[] singleVarNames = varNames.split(delimiter);
			
			assert(singleArguments.length == singleVarNames.length);

			for(int i=0; i<singleArguments.length; i++)			//IMPORTANT: assumption: one varName has only one argument (they are simple getters and setters)
			{
				String argument = singleArguments[i];
				String varName = singleVarNames[i];
				
				if(!argument.isEmpty() && !varName.isEmpty())
				{
					Method[] methods = instance.getClass().getMethods();
					
					for(Method method: methods)
					{
						String methodName = method.getName();
						
						String methodNameLookup = "set" + Character.toUpperCase(varName.charAt(0)) + varName.substring(1);
						
						if(methodName.equals(methodNameLookup))
						{
							try
							{
								Class[] parameterClasses = method.getParameterTypes();
								assert(parameterClasses.length==1);
								Class inputClass = parameterClasses[0];
								
								Object param = null;
								Class wrapperClass = inputClass;
								
								String primitiveClassName = inputClass.getName();
								String wrapperClassName = primitiveClassName;
								
								if(primitiveClassName.startsWith("java"))
								{
									//is already a wrapper class of a primitive
									 wrapperClassName = primitiveClassName;
								}
								else
								{
									wrapperClassName = "java.lang." + Character.toUpperCase(primitiveClassName.charAt(0))+ primitiveClassName.substring(1);
								}
							
								try
								{
									wrapperClass = Class.forName(wrapperClassName);
								}
								catch(ClassNotFoundException e)
								{
									logger.info("could not found wrapperClass " + wrapperClassName);
								}
								
								if(Number.class.equals(wrapperClass.getSuperclass())) //Try a number cast
								{
									//get name of the specific number class
									String numberClassName = wrapperClass.getSimpleName();
									String parserMethodName = "parse" + numberClassName;
									
									//find the parse method
									Method parserMethod = wrapperClass.getDeclaredMethod(parserMethodName, new Class[]{String.class}); 
									param = parserMethod.invoke(null, argument); //first argument is null, because it's a static method 
								}
								else
								{
//									logger.debug("try to cast the normal way");
									param = inputClass.cast(argument); //Try a simple cast
								}
								
								method.invoke(instance, param);
								break;
							} 
							catch (IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) 
							{
								logger.error(e.getMessage(),e);
							}
						}
					}
				}
				else
				{
//					logger.debug("Method and/or varName was empy");
				}
			}
			
			ICommand cmd = (ICommand) instance;
			cmd.setLiteral(symbol);
			
			return cmd;
		} 
		catch (InstantiationException | IllegalAccessException | ClassNotFoundException e)
		{
			logger.error(e.getMessage(),e);
			System.exit(1);
		}
		
		return null;
	}
	
	public final String getName()
	{
		return this.name;
	}
	
	public JSONObject getRules()
	{
		return this.rules;
	}
	
	public abstract List<ICommand> interpret(T input) throws Exception;
	
	protected abstract Evolution interpret(T input, int evolutionNumber) throws Exception;
	
	public abstract Evolution[] createEvolutions(T[] rawEvolutions) throws Exception;
}