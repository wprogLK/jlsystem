package core.interpreters;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import core.command.ICommand;
import core.evolution.Evolution;

/**
 * @author Lukas Keller
 * @author Kyriakos Schwarz 
 * @version 2.4
 */
public class SimpleInterpreter extends AbstractInterpreter<String>
{
	private static Logger logger = LogManager.getLogger(SimpleInterpreter.class);
	
	public SimpleInterpreter()
	{
		super();
	}
	
	public SimpleInterpreter(String filePath)
	{
		super(filePath);
	}
	
	public List<ICommand> interpret(String input)
	{
		/*
		 * invariant: the input string contains ONLY existing commands!!
		 */
		
		List<ICommand> commands = new ArrayList<ICommand>();
		
		List<String> separetedCommands = SimpleInterpreter.separateCommands(input, this.commands);
		
		for(String command:separetedCommands)
		{
			commands.add(this.findCommand(command));
		}
		
		return commands;
	}
	
	public Evolution interpret(String input, int evolutionNumber)
	{
		List<ICommand> commands = this.interpret(input);
		
		Evolution evolution = new Evolution(input, commands, this.isPlant, evolutionNumber, this.getName()+" - simple");
		
		return evolution;
	}
	
	public Evolution[] createEvolutions(String[] rawEvolutions)
	{
		Evolution[] evolutions = new Evolution[rawEvolutions.length];
		
		for(int i=0;i<rawEvolutions.length;i++)
		{
			String rawEvolution = rawEvolutions[i];
			evolutions[i] = this.interpret(rawEvolution,i);
		}
		
		return evolutions;
	}
	
	public static List<String> separateCommands(String input, JSONObject commandsContainer)
	{
		/*
		 * invariant: the input string contains ONLY existing symbol!!
		 */
		
		List<String> symbols = new ArrayList<String>();
		
		String symbol = "";// + input.charAt(0);
		
		for(int c=0;c<input.length();c++) 				//FIX: Works now with symbols with size greater than only one char
		{
			symbol+= input.charAt(c);
			
			if(commandsContainer.containsKey(symbol))
			{
				//symbol complete -> found
				symbols.add(symbol);
				symbol="";
			}
			else
			{
				//symbol incomplete -> not found
			}
		}
		
		assert(symbol.isEmpty()); //last Symbol must always be found.
		
		return symbols;
	}
}