package core.interpreters;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.command.ICommand;
import core.evolution.Evolution;

/**
 * @author Lukas Keller
 * @version 1.0
 */
public class CodingCommandInterpreter extends AbstractInterpreter<List<ICommand>>
{
	private static Logger logger = LogManager.getLogger(CodingCommandInterpreter.class);
	
	public CodingCommandInterpreter()
	{
		super();
	}
	
	public CodingCommandInterpreter(String filePath)
	{
		super(filePath);
	}
	
	public List<ICommand> interpret(List<ICommand> encodedInput) throws Exception
	{
		//encoded input is already a list of ICommands...
		return encodedInput;
	}
	
	public Evolution interpret(List<ICommand> input, int evolutionNumber) throws Exception
	{
		//input is already a list of ICommands -> interpret is not necessary
		Evolution evolution = new Evolution(input.toString(), input, this.isPlant, evolutionNumber, this.getName()+" - coding"); //TODO: input.toString
		
		return evolution;
	}
	
	public Evolution[] createEvolutions(List<ICommand>[] rawEncodedEvolutions) throws Exception
	{
		Evolution[] evolutions = new Evolution[rawEncodedEvolutions.length];
		
		for(int i=0;i<rawEncodedEvolutions.length;i++)
		{
			List<ICommand> rawEncodedEvolution = rawEncodedEvolutions[i];
			evolutions[i] = this.interpret(rawEncodedEvolution,i); //TODO/FIXME/CHECKTHIS
		}
		
		return evolutions;
	}
}