package core.ether;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jogamp.newt.event.KeyEvent;
import com.jogamp.newt.event.MouseEvent;

import core.evolution.EvolutionIterator;
import ch.fhnw.ether.controller.DefaultController;
import ch.fhnw.ether.view.IView;

/**
 * @author Lukas Keller
 * @version 1.0
 */
public class LSystemController extends DefaultController
{	
	private static Logger logger = LogManager.getLogger(LSystemController.class);
	
	private LSystemWindow window;
	private final EvolutionIterator ITERATOR;
	
	private int sleep = 500;
	private int sleepStep = 10;
	
	private Thread thread;
	
	public LSystemController(EvolutionIterator iterator, LSystemWindow window) 
	{
		super();
		this.ITERATOR = iterator;
		this.window = window;
	
		this.createThread();
	}
	
	private void createThread()
	{
		
		this.thread = new Thread()
		{
			public void run()
			{
				while(true)
				{
					window.setEvolution(ITERATOR.nextEvolution());
					
					try
					{
						sleep(sleep);
					} 
					catch (InterruptedException e)
					{
						logger.error(e.getMessage(),e);
					}
				}
			}
		};
	}
	
	public void setWindow(LSystemWindow window)
	{
		this.window = window;
	}
	
	@Override
	public void keyPressed(KeyEvent e, IView view) 
	{
		int keyCode = e.getKeyCode();
		
		switch(keyCode)
		{
			case KeyEvent.VK_0:
			{
				this.ITERATOR.setCurrentEvolution(0);
				
				this.window.setEvolution(this.ITERATOR.currentEvolution());
				break;
			}
			case KeyEvent.VK_UP:
			{
				this.window.setEvolution(this.ITERATOR.nextEvolution());
				break;
			}
			case KeyEvent.VK_DOWN:
			{
				this.window.setEvolution(this.ITERATOR.prevEvolution());
				break;
			}
			case KeyEvent.VK_A:
			{
				this.ITERATOR.setCurrentEvolution(0);
				this.window.setEvolution(this.ITERATOR.currentEvolution());
				
				thread.start();
				break;
			}
			case KeyEvent.VK_S:
			{
				this.ITERATOR.setCurrentEvolution(0);
				
				thread.stop(); //FIXME//TODO
				this.createThread();
				break;
			}
			case KeyEvent.VK_LEFT:
			{
				logger.info("slow down animation");
				this.sleep+=this.sleepStep;
				break;
			}
			case KeyEvent.VK_RIGHT:
			{
				logger.info("speed up animation");
				this.sleep-=this.sleepStep;
				break;
			}
				
		}
		
		super.keyPressed(e, view);
	}

	@Override
	public void keyReleased(KeyEvent e, IView view)
	{
		super.keyReleased(e, view);
	}

	// mouse listener

	@Override
	public void mouseEntered(MouseEvent e, IView view)
	{
		super.mouseEntered(e, view);
	}

	@Override
	public void mouseExited(MouseEvent e, IView view)
	{
		super.mouseExited(e, view);
	}

	@Override
	public void mousePressed(MouseEvent e, IView view)
	{
		super.mousePressed(e, view);
	}

	@Override
	public void mouseReleased(MouseEvent e, IView view) 
	{
		super.mouseReleased(e, view);
	}

	@Override
	public void mouseClicked(MouseEvent e, IView view) 
	{
		super.mouseClicked(e, view);
	}

	// mouse motion listener

	@Override
	public void mouseMoved(MouseEvent e, IView view)
	{
		super.mouseMoved(e, view);
	}

	@Override
	public void mouseDragged(MouseEvent e, IView view) 
	{
		super.mouseDragged(e, view);
	}

	// mouse wheel listener

	@Override
	public void mouseWheelMoved(MouseEvent e, IView view) 
	{
		super.mouseWheelMoved(e, view);
	}
}