package core.ether;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.evolution.Evolution;
import core.evolution.EvolutionIterator;
import core.evolution.IEvolution;
import core.evolution.NullEvolution;
import core.model.ILSystem3DModel;
import ch.fhnw.ether.controller.DefaultController;
import ch.fhnw.ether.controller.IController;
import ch.fhnw.ether.formats.obj.OBJReader;
import ch.fhnw.ether.scene.DefaultScene;
import ch.fhnw.ether.scene.IScene;
import ch.fhnw.ether.scene.camera.Camera;
import ch.fhnw.ether.scene.camera.ICamera;
import ch.fhnw.ether.scene.light.DirectionalLight;
import ch.fhnw.ether.scene.light.ILight;
import ch.fhnw.ether.scene.light.PointLight;
import ch.fhnw.ether.scene.mesh.DefaultMesh;
import ch.fhnw.ether.scene.mesh.IMesh;
import ch.fhnw.ether.scene.mesh.MeshLibrary;
import ch.fhnw.ether.scene.mesh.geometry.DefaultGeometry;
import ch.fhnw.ether.scene.mesh.geometry.IGeometry;
import ch.fhnw.ether.scene.mesh.geometry.IGeometry.Primitive;
import ch.fhnw.ether.scene.mesh.material.ColorMaterial;
import ch.fhnw.ether.scene.mesh.material.IMaterial;
import ch.fhnw.ether.scene.mesh.material.ShadedMaterial;
import ch.fhnw.ether.scene.mesh.material.Texture;
import ch.fhnw.ether.view.IView;
import ch.fhnw.ether.view.gl.DefaultView;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.color.RGBA;
import ch.fhnw.util.math.Vec3;
import ch.fhnw.util.math.geometry.GeometryUtil;

/**
 * @author Lukas Keller, Patric Sigrist
 * @version 1.6
 */
public class LSystemWindow 
{
	private static Logger logger = LogManager.getLogger(LSystemWindow.class);
	
	private final Vec3 CAMERA_POSITION = new Vec3(1,-15,5);
	private final Vec3 CAMERA_UP = new Vec3(0,0,1);
	
	private final int VIEW_POS_X = 100;
	private final int VIEW_POS_Y = 100;
	private final int VIEW_WIDTH = 900;
	private final int VIEW_HEIGHT = 900;
	
	private static final RGB AMBIENT = RGB.BLACK;
	private static final RGB COLOR = RGB.WHITE;
	
	private final ILight dirlight = new DirectionalLight(Vec3.Z, AMBIENT, COLOR);
	private final ILight plight = new PointLight(Vec3.Z, AMBIENT, COLOR);
	
	private final IView.Config  VIEW_TYPE = IView.INTERACTIVE_VIEW;
	
	private ICamera camera;
	private IScene scene;
	private IController controller;
	private IView view;
	
	private IEvolution evolution;
	private IMesh mesh;

	private Vec3 dirLightPosition = new Vec3(-2,-4,10);
	private Vec3 pLightPosition = new Vec3(10,5,0);
	
	private final static IController DEFAULT_CONTROLLER = new DefaultController();
	
	private List<IMesh> currentLeafs;
	
	private int changeEvolution = 0;
	
	private boolean groundSet = false;
	/**
	 * 
	 */
	public LSystemWindow(String name) 
	{
		this.init(name, LSystemWindow.DEFAULT_CONTROLLER);
	}
	
	public LSystemWindow(String name, EvolutionIterator iterator)
	{
		LSystemController controller = new LSystemController(iterator,this);
		this.init(name, controller);
	}
	
	private void addLight() {
		this.plight.setPosition(this.pLightPosition);
		this.dirlight.setPosition(this.dirLightPosition);
		scene.add3DObjects(this.plight, this.dirlight);
	}
	
	public void init(String name, IController controller)
	{
		this.setupCamera();
		
		this.controller = controller;
		this.scene = new DefaultScene(this.controller);
		
		this.view = new DefaultView(this.controller,this.VIEW_POS_X,this.VIEW_POS_Y, this.VIEW_WIDTH, this.VIEW_HEIGHT, this.VIEW_TYPE,name,this.camera);
		this.controller.addView(this.view);
		this.controller.setScene(this.scene);	
		
		this.addLight();
		
		this.currentLeafs = new LinkedList<IMesh>();
	}
	
	private void addGround(){
//	    RGBA rgba = RGBA.GREEN; // light green 
//	    RGBA rgba = new RGBA(0, 0.2f, 0, 1); // green
//	    RGBA rgba = new RGBA(0, 0.1f, 0, 1); // dark green

	    float size = 400;
	    float[] vertices = { -size, -size, 000.1f, size, size, 000.1f, -size, size, 000.1f};
	    float[] vertices2 = { -size, -size, 000.1f, size, -size, 000.1f, size, size, 000.1f};
//	    float[] colors = { 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 1 };
	    float[] colors = new float[vertices.length];
	    for(int i=0; i<vertices.length; i++){
	        colors[i] = 0;
	    }
	    
	    float[] normals = GeometryUtil.calculateNormals(vertices);
	    float[] normals2 = GeometryUtil.calculateNormals(vertices2);
	    
//	    DefaultGeometry g = DefaultGeometry.createVC(Primitive.TRIANGLES, vertices, colors);
//	    DefaultGeometry g2 = DefaultGeometry.createVC(Primitive.TRIANGLES, vertices2, colors);
//	    IMesh ground = new DefaultMesh(new ColorMaterial(rgba), g);
//      IMesh ground2 = new DefaultMesh(new ColorMaterial(rgba), g2);
	    DefaultGeometry geometry = DefaultGeometry.createVNC(Primitive.TRIANGLES, vertices, normals, colors);
	    DefaultGeometry geometry2 = DefaultGeometry.createVNC(Primitive.TRIANGLES, vertices2, normals2, colors);
//	    IMesh ground = new DefaultMesh(new ColorMaterial(rgba), g);
//      IMesh ground2 = new DefaultMesh(new ColorMaterial(rgba), g2);
	    RGB groundColor = new RGB(0, 0.2f, 0); // green
	    RGB groundColorRef = RGB.GRAY10;
	    IMaterial shadedGroundMaterial = new ShadedMaterial(RGB.BLACK, RGB.BLACK, groundColor, groundColorRef, 10, 1, 1f);
	    IMesh ground = new DefaultMesh(shadedGroundMaterial, geometry);
        IMesh ground2 = new DefaultMesh(shadedGroundMaterial, geometry2);
	    
	    this.scene.add3DObject(ground);
	    this.scene.add3DObject(ground2);
	}
	
	private void addLeaf(Vec3 v)
	{
		try {
			
			List<IMesh> meshes = new OBJReader(getClass().getResource("assets/leafWithOrigin.obj")).getMeshes();
			Texture texture = new Texture(getClass().getResource("assets/leaf.jpg"));
			IMaterial textureMaterial = new ShadedMaterial(RGB.BLACK, RGB.BLUE, RGB.GRAY, RGB.YELLOW, 10, 1, 1f, texture);
			
			double zAngle = Math.toDegrees(Math.atan2((v.y() - 0), (v.x() - 1))) + 180 + this.randInt(-25, 25);
			double yAngle = this.randInt(-25, 25);
//			double xAngle = 90 + this.randInt(-15, 15);
			
			IGeometry leafGeometry = meshes.get(0).getGeometry();
			float scaleFactor = (float) 0.05;
			leafGeometry.setScale(new Vec3(scaleFactor,scaleFactor,scaleFactor));
			//leafGeometry.setOrigin(new Vec3(10,10,10));
			leafGeometry.setRotation(new Vec3(90,zAngle,yAngle));
			leafGeometry.setTranslation(v);
			
			IMesh leaf = new DefaultMesh(textureMaterial, leafGeometry);
			this.currentLeafs.add(leaf);
			this.scene.add3DObject(leaf);
		} catch (IOException e) {
			logger.error(e.getMessage(),e);
			System.exit(-1);
		}
		
	}
	
	private int randInt(int min, int max) {

	    // NOTE: Usually this should be a field rather than a method
	    // variable so that it is not re-seeded every call.
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
	
	public void setController(IController controller)
	{
		this.controller.removeView(this.view);

		this.controller = controller;
		
		this.controller.addView(this.view);
		this.controller.setScene(this.scene);
	}

	public void setEvolution(Evolution evolution)
	{
		if(this.evolution!=evolution)
		{
			this.changeEvolution++;
			
			int changedEvolution = this.changeEvolution;
			this.evolution = evolution;
			this.scene.remove3DObject(this.mesh);  //remove the old mesh
//			this.scene.remove3DObjects(this.currentLeafs); //remove old meshes
			
			//FIXME!!!!! Fenster h�ngt sich nach Iterationswechel auf!
			
			for(IMesh m : this.currentLeafs)
			{
				this.scene.remove3DObject(m);
				if(this.changeEvolution!=changedEvolution)
				{
					//altes setzten wird unterbrochen resp. abgebrochen da evolution neu gesetzt worden ist....
					logger.debug("BREAK CLEAN");
				
					return;
				}
			}
			
			ILSystem3DModel model = this.evolution.getModel();
			
			if(!this.groundSet && model.isPlant())
			{
				this.addGround();
			}
			
			this.addModel(model);
			
			
			List<Vec3> leafTargets = model.getEndNodes();
			this.currentLeafs.clear();
			
			for (Vec3 v: leafTargets) {
					if(this.changeEvolution!=changedEvolution)
					{
						//altes setzten wird unterbrochen resp. abgebrochen da evolution neu gesetzt worden ist....
						logger.debug("BREAK ADD");
					
						return;
					}
					
					this.addLeaf(v);
					this.view.repaint();
			}
			
			
			logger.debug("DONE");
		}
		else
		{
			logger.info("Not a new evolution... no repaint necessary...");
		}
	}
	
	public void addModel(ILSystem3DModel model)
	{
		this.mesh = model.createMesh();
		
		
		if(this.mesh!=null)
		{
			this.scene.add3DObjects(this.mesh);
		}
		else
		{
			logger.info("Mesh of the evolution " + evolution.getEvolutionNumber() +" was null...");
		}
		
		this.view.repaint();
	}
	
	private void setupCamera() 
	{
		this.camera = new Camera();
		this.camera.setPosition(this.CAMERA_POSITION);
		this.camera.setUp(this.CAMERA_UP);
	}
}