package core.generators;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import core.coders.ICoder;

/**
 * Generates the all raw evolutions as an array of strings.
 * @author Kyriakos Schwarz
 * @author Lukas Keller
 * @version 2.1
 */
public abstract class AbstractGenerator<T> implements IGenerator<T>
{
	private static Logger logger = LogManager.getLogger(AbstractGenerator.class);
	
	protected T[] iterations;
	
	protected final int MAX_EVOLUTION;
	
	protected final String FILE_PATH;
	
	protected String result;
	protected String tmp;
	private JSONObject jsonObject;
	private JSONObject rules;
	private JSONObject commands;
	
	protected ICoder<T> coder;
	
	/**
	 * @throws ParseException 
	 * @throws IOException 
	 * 
	 */
	public AbstractGenerator(String filePath, int maxEvolution)
	{
		this.MAX_EVOLUTION = maxEvolution;
		this.FILE_PATH = filePath;
		
		this.iterations = this.createArray(maxEvolution);
		this.init();
	}
	
	protected abstract T[] createArray(int size);
	
	
	private final void init()
	{
		try
		{
			FileReader reader = new FileReader(this.FILE_PATH);
			JSONParser jsonParser = new JSONParser();
			jsonObject = (JSONObject) jsonParser.parse(reader);
	        
	        this.result = getInitial();
	        this.commands = (JSONObject) jsonObject.get("commands");
	        this.rules = (JSONObject) jsonObject.get("rules");	//TODO/FIXME/HANDLE THIS: It's not allowed to end a rule with a terminal symbol, otherwise the generator will throw an exception!
	        
	      	this.tmp = "";
	      	
	      	this.coder = this.createCoder(this.rules, this.commands);
		} 
		catch (IOException | ParseException e)
		{
			logger.error(e.getMessage(),e);
			System.exit(1);
		}
	}
	
	protected abstract ICoder<T> createCoder(JSONObject rules, JSONObject commands);
	
	protected String getInitial()
	{
		String inital = (String) this.jsonObject.get("initial");
		return inital;
	}
	
	public abstract T[] generate();
	
	private final void removeNonKeys ()
	{
		while (!charInKeys(result.charAt(0))) 
		{
			tmp += result.charAt(0);
			result = result.substring(1, result.length());
		}
	}
	
	private final boolean charInKeys (char c)
	{
		Set keys = rules.keySet();
        Iterator it = keys.iterator();
        boolean has = false;
        while(it.hasNext())
        {
        	String key = (String) it.next();
            if (key.charAt(0) == c)
            {
            	has = true;
            	break;
            }
        }
        return has;
	}
	
	private final String findNextKey () 
	{
		removeNonKeys();
		
		int end = 1;
		String curr = result.substring(0, end);

		while (!this.coder.existsRule(curr))
		{
			end++;
			curr = result.substring(0, end);
		}		
		return curr;
	}
	
	private final void replaceKey (String key)
	{
		String toadd = (String) rules.get(key);
		tmp += toadd;
		result = result.substring(key.length(), result.length());
	}
	
	protected final void replaceAllKeys()
	{
		tmp = "";
		while (result.length() > 0) 
		{
			replaceKey(findNextKey());
		}
		result = tmp;
		tmp = "";
	}
}
