package core.generators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import core.coders.CommandCoder;
import core.coders.ICoder;
import core.command.ICommand;
import core.interpreters.SimpleInterpreter;

/**
 * Generates the all raw evolutions as an array of strings.
 * @author Lukas Keller
 * @version 1.1
 */
public class CodingCommandGenerator extends AbstractGenerator<List<ICommand>>
{
	private static Logger logger = LogManager.getLogger(CodingCommandGenerator.class);
	
	private SimpleInterpreter interpreter;
	
	/**
	 * @throws ParseException 
	 * @throws IOException 
	 * 
	 */
	public CodingCommandGenerator(String filePath, int maxEvolution)
	{
		super(filePath, maxEvolution);
		this.interpreter = new SimpleInterpreter(filePath);
		
	}
	
	@Override
	protected List<ICommand>[] createArray(int size) {
		return new List[size];
	}
	
	@Override
	public List<ICommand>[] generate()
	{
		
		String start = this.getInitial();
		
		try 
		{
			List<ICommand> basic = this.interpreter.interpret(start);

			this.iterations[0] = basic;
			for(int i=1; i<this.MAX_EVOLUTION;i++)
			{
				List<ICommand> last = this.iterations[i-1];
				
				this.iterations[i] = new ArrayList<ICommand>();
				
				for(ICommand cmd: last)
				{
//					String cmdName = cmd.getLiteral();
//					
//					if(this.interpreter.getRules().containsKey(cmdName))
//					{
//						String rule = (String) this.interpreter.getRules().get(cmdName);
//						List<ICommand> cmdRules = this.interpreter.interpret(rule); //TODO: optimize!
//						cmd.setRules(cmdRules);
//					}
//					
//					cmd.apply(this.iterations[i]);
					
					cmd.apply(this.iterations[i]);
				}
				
				logger.info("Evolution "+ i +" done!");
			}
		} 
		catch (Exception e) 
		{
			logger.error(e.getMessage(), e);
			System.exit(-1);
		}
		logger.info("DONE!");
		
        return this.iterations;
	}
	
	
	protected ICoder<List<ICommand>> createCoder(JSONObject rules, JSONObject commands)
	{
		return new CommandCoder(rules, commands);
	}
}