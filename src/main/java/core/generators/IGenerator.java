package core.generators;

/**
 * @author Lukas Keller
 * @version 1.1
 */
public interface IGenerator<T>
{
	public T[] generate();
}
