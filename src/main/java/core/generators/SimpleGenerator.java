package core.generators;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import core.coders.StringCoder;
import core.coders.ICoder;

/**
 * Generates the all raw evolutions as an array of strings.
 * @author Kyriakos Schwarz
 * @version 1.7
 */
public class SimpleGenerator extends AbstractGenerator<String>
{
	private static Logger logger = LogManager.getLogger(SimpleGenerator.class);
	
	/**
	 * @throws ParseException 
	 * @throws IOException 
	 * 
	 */
	public SimpleGenerator(String filePath, int maxEvolution)
	{
		super(filePath, maxEvolution);
	}
	
	@Override
	protected String[] createArray(int size) {
		return new String[size];
	}
	
	@Override
	public String[] generate() 
	{            
		this.iterations[0] = this.result;
		
        for (int i = 1; i < this.MAX_EVOLUTION; i++) 
        {
        	replaceAllKeys();
        	this.iterations[i] = result;
        	try 
        	{
				this.coder.encode(result);
			} 
        	catch (Exception e) 
        	{
				logger.error(e.getMessage(), e);
				System.exit(-1);
        	}
        }
        
        return this.iterations;
	}
	
	protected ICoder<String> createCoder(JSONObject rules, JSONObject commands)
	{
		return new StringCoder(rules, commands);
	}
}