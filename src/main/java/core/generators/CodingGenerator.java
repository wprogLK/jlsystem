package core.generators;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import core.coders.StringCoder;
import core.coders.ICoder;

/**
 * Generates the all raw evolutions as an array of strings.
 * @author Lukas Keller
 * @version 1.6
 */
public class CodingGenerator extends AbstractGenerator<String>
{
	private static Logger logger = LogManager.getLogger(CodingGenerator.class);
	
	/**
	 * @throws ParseException 
	 * @throws IOException 
	 * 
	 */
	public CodingGenerator(String filePath, int maxEvolution)
	{
		super(filePath, maxEvolution);
	}
	
	@Override
	protected String[] createArray(int size) {
		return new String[size];
	}
	
	@Override
	public String[] generate()
	{
		try 
    	{
			this.iterations[0] = this.coder.encode(this.result);
			
	        for (int i = 1; i < this.MAX_EVOLUTION; i++) 
	        {
	        	replaceAllKeys();
	        	this.iterations[i] = this.coder.encode(this.result);
	        }
    	} 
    	catch (Exception e) 
    	{
			logger.error(e.getMessage(), e);
			System.exit(-1);
    	}
		
        return this.iterations;
	}
	
	protected ICoder<String> createCoder(JSONObject rules, JSONObject commands)
	{
		return new StringCoder(rules, commands);
	}
}