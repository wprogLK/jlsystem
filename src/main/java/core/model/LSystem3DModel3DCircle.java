/**
 * 
 */
package core.model;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.fhnw.util.math.Vec2;
import ch.fhnw.util.math.Vec3;

/**
 * @author Lukas Keller
 * @version 1.0
 */
public class LSystem3DModel3DCircle extends LSystem3DModel2D
{
	private static Logger logger = LogManager.getLogger(LSystem3DModel3DCircle.class);
	
	/*DO NOT CHANGE THESE VALUES*/
	private final double ALLOWED_ANGLE = 20; //in deg
	private final double FULL_CIRCLE = 360.0; 
	
	
	/*THESE VALUES CAN BE CHANGED*/
	private final int RESOLUTION = 40;	//= CIRCLE RESOLUTION
	private final double ANGLE = 360; //in deg
	private final double OFFSET_ANGLE = 0;
	
	public LSystem3DModel3DCircle()
	{
		super();
	}
	
	public LSystem3DModel3DCircle(boolean isPlant)
	{
		super(isPlant);
	}
	
	protected double getAngle()
	{
		Vec3 p = new Vec3(1,1,1);
		Vec3 pRot = this.applyRotation(p);
		
		float scalar = p.dot(pRot);
		
		double angle = Math.toDegrees(Math.acos(scalar/(p.length()*pRot.length())));
		
		return angle;
	}
	
	@Override
	protected Vec3[] createBranchSegment(Vec3 targetCenter, float diameter)
	{
		//TODO IMPROVE IT!
		
		double angle = ANGLE;
		double dt = computeFactorizedDt(RESOLUTION,angle);
		
		float radius = diameter/2;
		
		double x = -radius;
		double y = -radius;
		double z = 0;
		
		double offsetAngle = OFFSET_ANGLE;
		
		Vec3 v0 = new Vec3(x, y,z); //TODO: Test this/check this!
		
		Vec3[] vertices = new Vec3[RESOLUTION];
		Vec3[] segment = new Vec3[RESOLUTION+1];
		
		vertices[0] = v0;
		
		for(int i=0;i<RESOLUTION;i++)
		{
			double vx = computeCircleX(radius, i, dt, x, offsetAngle);
			double vy = computeCircleY(radius,i,dt,y,offsetAngle);
			double vz = z;
			
			vertices[i] = new Vec3(vx,vy,vz);
		}
		
		//**********//
		
		double calcAngle = this.getAngle();
		
		if(calcAngle>-ALLOWED_ANGLE && angle<ALLOWED_ANGLE)
		{
			for(int i=0;i<RESOLUTION;i++)
			{
				vertices[i]=this.applyRotation(vertices[i]);
			}
		}
		else
		{
			//-
		}
		
		for(int i=0;i<RESOLUTION;i++)
		{
			segment[i]=vertices[i].add(targetCenter);
		}
		
		segment[RESOLUTION]=segment[0];
		
		return segment;
	}
	
	private double computeFactorizedDt(double resolution, double angle)
	{
		double factor = FULL_CIRCLE/angle;
		return computeDt(resolution*factor);
	}
	
	private static double computeDt(double resolution)
	{
		return 2.0*Math.PI/resolution;
	}
	
	private static double computeCircleX(double radius, int i, double dt, double x, double angle)
	{
		return radius*Math.cos(i*dt-Math.toRadians(angle))+x;
	}
	
	private static double computeCircleY(double radius, int i, double dt, double y, double angle)
	{
		return radius*Math.sin(i*dt-Math.toRadians(angle))+y;
	}
	
	protected void popMemory()
	{
		addEndFace();
		super.popMemory();
	}
	
	protected void addEndFace()
	{
//		List<Vec3> endFace = this.createPlane(this.lastSegment[0], this.lastSegment[1], this.lastSegment[3], this.lastSegment[2]);
//		this.vertices.addAll(endFace);
		List<Vec3> endFace = this.createFilledCirlce(this.lastSegment);
		this.vertices.addAll(endFace);
	}
	
	protected List<Vec3> createFilledCirlce(Vec3[] segment)
	{
		//TODO
		//FIXME
		
		List<Vec3> circle = new ArrayList<Vec3>();
		return circle;
	}
}