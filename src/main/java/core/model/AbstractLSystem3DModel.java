package core.model;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.fhnw.ether.scene.mesh.DefaultMesh;
import ch.fhnw.ether.scene.mesh.IMesh;
import ch.fhnw.ether.scene.mesh.geometry.DefaultGeometry;
import ch.fhnw.ether.scene.mesh.geometry.IGeometry.Primitive;
import ch.fhnw.ether.scene.mesh.material.ColorMaterial;
import ch.fhnw.ether.scene.mesh.material.ShadedMaterial;
import ch.fhnw.ether.scene.mesh.material.IMaterial;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.color.RGBA;
import ch.fhnw.util.math.Mat4;
import ch.fhnw.util.math.Vec3;

/**
 * @author Lukas Keller, Patric Sigrist
 * @version 1.5
 */
public abstract class AbstractLSystem3DModel implements ILSystem3DModel
{
	private static Logger logger = LogManager.getLogger(AbstractLSystem3DModel.class);
	
	private static final Vec3 ORIGIN_MAT = new Vec3(0,0,0);
	
	private Mat4 rotation;
	private Mat4 translate;
	
	private static final float DEFAULT_LENGTH = 1.0f;
	private float drawLength;
	
	protected List<Vec3> vertices;
	
	private final static Vec3 DEFAULT_ORIGIN = new Vec3(0,0,0);
	protected final Vec3 ORIGIN;
	
	private Stack<Mat4> rotationStack = new Stack<Mat4>();
	private Stack<Mat4> translateStack = new Stack<Mat4>();
	private Stack<Float> drawLengthStack = new Stack<Float>();
	
	protected IMesh mesh;
	
	private final boolean CENTERED = true;
	private final boolean SCALED = true;
	
	private final Vec3 SCALE = new Vec3(0.1, 0.1, 0.1);
	
	private boolean isPlant = false;
	
	public AbstractLSystem3DModel()
	{
		this(DEFAULT_ORIGIN);
	}
	
	public AbstractLSystem3DModel(boolean isPlant)
	{
		this(DEFAULT_ORIGIN, isPlant);
	}
	
	public AbstractLSystem3DModel(Vec3 origin)
	{
		this.rotation = Mat4.ID;
		this.translate = Mat4.ID;
		this.translate = this.translate.translate(ORIGIN_MAT);
		this.ORIGIN = origin;
		this.vertices = new LinkedList<Vec3>();
		this.drawLength = DEFAULT_LENGTH;
	}
	
	public AbstractLSystem3DModel(Vec3 origin, boolean isPlant)
	{
		this.rotation = Mat4.ID;
		this.translate = Mat4.ID;
		this.translate = this.translate.translate(ORIGIN_MAT);
		this.ORIGIN = origin;
		this.vertices = new LinkedList<Vec3>();
		this.drawLength = DEFAULT_LENGTH;
		this.isPlant = isPlant;
	}
	
	public IMesh createMesh() 
	{
		if(this.mesh==null)
		{	
			logger.info("Create mesh...");
			
			float[] position = Vec3.toArray(this.vertices);
			float[] colors = new float[position.length];
			
			for(int i = 0; i < position.length; i++) {
				colors[i] = 1; 
			}
			
			logger.debug("vertices: " + this.vertices.toString());
			logger.debug("nr of verticse: " + this.vertices.size());
			
			logger.debug("pos: " + Arrays.toString(position));
			logger.debug("nr of pos: " + position.length);
			
			DefaultGeometry geometry = DefaultGeometry.createVC(Primitive.LINES, position, colors);

			this.mesh = new DefaultMesh(this.getMaterial(), geometry);
			
			mesh = this.centralize(mesh);
			this.scale(mesh);
			
			logger.info("... done!");
		}
		
		return this.mesh;
	}
	
	protected IMaterial getMaterial() {
		return new ShadedMaterial(RGB.BLACK, RGB.GREEN, RGB.MAGENTA, RGB.WHITE, 10, 1, 1f);
	}
	
	@Override
	public List<Vec3> getEndNodes() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public abstract void addNodeAt(Vec3 node);
	
	protected abstract void popMemory();
	protected abstract void pushMemory();
	
	protected final void addOrigin() 
	{
		assert(this.vertices.isEmpty());
		addNodeAt(this.ORIGIN);
	}
	
	/**
	 * 
	 * @param angle in degree
	 */
	public final void rotate(float angle)
	{
		Vec3 axis = new Vec3(0,0,1); //rotation um z achse
		this.rotation=this.rotation.rotate(angle, axis);
	}
	
	public final void rotate(float angle, Vec3 axis){
	    this.rotation=this.rotation.rotate(angle, axis);
	       
//	    this.rotation=this.rotation.rotate(angle, this.calculate(axis));
	}
	
	public final void rotateTurtle(float angle, Vec3 axis)
	{	    
	    if(axis.equals(new Vec3(1,0,0))){
	        Vec3 rotationAxis = new Vec3(this.rotation.m00, this.rotation.m10, this.rotation.m20);
	        this.rotation=this.rotation.rotate(angle, rotationAxis);
	    }
	    else if(axis.equals(new Vec3(0,1,0))){
	        Vec3 rotationAxis = new Vec3(this.rotation.m01, this.rotation.m11, this.rotation.m21);
	        this.rotation=this.rotation.rotate(angle, rotationAxis);
	    }
	    else if(axis.equals(new Vec3(0,0,1))){
	        Vec3 rotationAxis = new Vec3(this.rotation.m02, this.rotation.m12, this.rotation.m22);
	        this.rotation=this.rotation.rotate(angle, rotationAxis);
	    }
	    else{
	        System.out.println("Error? Unexpected axis in rotateTurtle()");
	    }
	}
	
	public final void multiplyDrawLength(float factor)
	{
	    this.drawLength=this.drawLength*factor;
	}
	
	public final void translate(Vec3 vec)
	{
//		Vec3 rotated = this.rotation.transform(vec);
	    Vec3 rotated = this.rotation.transform(vec.scale(this.drawLength));
		this.translate = this.translate.translate(rotated);
	}
	
	public final void push()
	{
		this.rotationStack.push(this.rotation);
		this.translateStack.push(this.translate);
		this.drawLengthStack.push(this.drawLength);
		
		this.pushMemory();
	}
	
	public final void pop()
	{
		this.rotation = this.rotationStack.pop();
		this.translate= this.translateStack.pop();
		this.drawLength = this.drawLengthStack.pop();
		
		this.popMemory();
	}
	
	protected final Vec3 calculate(Vec3 vertex)
	{
		Vec3 rotated = applyRotation(vertex);
		Vec3 v = applyTranslation(rotated);
	    
		return v;
	}
	
	protected final Vec3 applyRotation(Vec3 vertex)
	{
		return this.rotation.transform(vertex);
	}
	
	protected final Vec3 applyTranslation(Vec3 vertex)
	{
		return this.translate.transform(vertex);
	}
	
	public final int getNumberOfVertices()
	{
		return this.vertices.size();
	}
	
	public final boolean isPlant()
	{
		return this.isPlant;
	}
	
	private IMesh centralize(IMesh mesh)
	{
		if(this.CENTERED)
		{
			//TODO
		}
		
		return mesh;
	}
	
	private void scale(IMesh mesh)
	{
		if(this.SCALED)
		{
			mesh.getGeometry().setScale(SCALE);
		}
	}
}
