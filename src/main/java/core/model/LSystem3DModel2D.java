/**
 * 
 */
package core.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.fhnw.ether.scene.mesh.DefaultMesh;
import ch.fhnw.ether.scene.mesh.IMesh;
import ch.fhnw.ether.scene.mesh.geometry.DefaultGeometry;
import ch.fhnw.ether.scene.mesh.geometry.IGeometry.Primitive;
import ch.fhnw.ether.scene.mesh.material.ColorMaterial;
import ch.fhnw.ether.scene.mesh.material.IMaterial;
import ch.fhnw.ether.scene.mesh.material.ShadedMaterial;
import ch.fhnw.util.color.RGB;
import ch.fhnw.util.color.RGBA;
import ch.fhnw.util.math.Vec3;
import ch.fhnw.util.math.geometry.GeometryUtil;

/**
 * @author Lukas Keller
 * @version 1.2
 */
public class LSystem3DModel2D extends AbstractLSystem3DModel
{
	private static Logger logger = LogManager.getLogger(LSystem3DModel2D.class);
	
	protected float branchDiameter = 0.1f;
	
	protected Vec3[] lastSegment = null;
	protected Stack<Vec3[]> lastSegmentStack = new Stack<Vec3[]>();
	
	protected Vec3 target;
	
	public ArrayList<Vec3> leafTargets = new ArrayList<Vec3>();
	
	protected boolean lastWasAPush = false;
	
	private int vertexNumberOfASegment = 2;
	
	public LSystem3DModel2D()
	{
		super();
		this.addOrigin();
	}
	
	public LSystem3DModel2D(boolean isPlant)
	{
		super(isPlant);
		this.addOrigin();
	}
	
	@Override
	public IMesh createMesh() 
	{
		if(this.mesh==null)
		{
			logger.info("Create mesh ...");
			
			float[] triangles = Vec3.toArray(this.vertices);
			float[] normals = GeometryUtil.calculateNormals(triangles);
			
			float[] colors = new float[triangles.length];
			
			for(int i = 0; i < triangles.length; i++) {
				colors[i] = 0; 
			}
			
//			logger.debug("vertices: " + this.vertices.toString());
//			logger.debug("nr of verticse: " + this.vertices.size());
//			
//			logger.debug("pos: " + Arrays.toString(position));
//			logger.debug("nr of pos: " + position.length);
			
			DefaultGeometry geometry = DefaultGeometry.createVNC(Primitive.TRIANGLES, triangles, normals, colors);

//			RGBA rgba = new RGBA(0.445f, 0.27f, 0.074f, 1); // BROWN
//			RGBA rgba = new RGBA(0.3f, 0.2f, 0.1f, 1); // light brown
//			RGBA rgba = new RGBA(0.15f, 0.1f, 0.05f, 1); // dark brown
//			IMaterial material = new ColorMaterial(rgba);
			
//			IMaterial material = super.getMaterial();
			RGB rgb = new RGB(0.3f, 0.2f, 0.1f); // light brown
//			RGB rgb = new RGB(0.15f, 0.1f, 0.05f); // dark brown
//	       RGB rgb = RGB.GREEN; // light green 
//	        RGB rgb = new RGB(0, 0.2f, 0); // green
//	      RGB rgb = new RGB(0, 0.1f, 0); // dark green
//			RGB rgb = new RGB(0.9f, 0.9f, 0.1f); // yellow-green
			
//			IMaterial material = new ShadedMaterial(RGB.BLACK, RGB.GREEN, rgb, RGB.WHITE, 10, 1, 1f);
//			IMaterial material = new ShadedMaterial(rgb);
//			float factor = 1.5f;
//			RGB rgbRef = new RGB(rgb.r*factor, rgb.b*factor, rgb.g*factor); 
//			float summand = -0.05f;
//			RGB rgbRef = new RGB(rgb.r+summand, rgb.b+summand, rgb.g+summand);
			RGB rgbRef = RGB.GRAY30;
			IMaterial material = new ShadedMaterial(RGB.BLACK, RGB.BLACK, rgb, rgbRef, 10, 1, 1f);
			
//			IMaterial material = new ShadedMaterial(RGB.BLACK, RGB.GREEN, RGB.MAGENTA, RGB.WHITE, 10, 1, 1f);
			
			this.mesh = new DefaultMesh(material, geometry);
			
			logger.info("... done!");
		}
		
		return this.mesh;
	}
	
	public void addBranch(Vec3 vertex)
	{
//		if(this.lastSegment!=null) //not at the beginning
//		{
////			this.addBranchSegment(this.lastSegment);
//		} 
//		
//		Vec3 target = this.calculate(vertex);
////		logger.info("target:: " + target);
//		Vec3[] branchSegment = this.createBranchSegment(target,this.branchDiameter);
//		
//		this.addBranchSegment(branchSegment);
//		this.lastSegment = branchSegment;
		
		target = this.calculate(vertex);
		Vec3[] branchSegment = this.createBranchSegment(target,this.branchDiameter);
		
		if(this.lastSegment!=null)
		{
			List<Vec3> faces = this.createFaces(this.lastSegment, branchSegment);
			
			this.vertices.addAll(faces);
			
//			this.addBranchSegment(branchSegment);
		}
		
		this.lastSegment = branchSegment;
	}
	
	private void addBranchSegment(Vec3[] segment)
	{
		for(Vec3 vec: segment)
		{
			this.vertices.add(vec);
		}
	}
	
	private List<Vec3> createFaces(Vec3[] lastSegment, Vec3[] newSegment)
	{
		assert(lastSegment.length == newSegment.length);
		
		List<Vec3> faces = new ArrayList<Vec3>();

		int i=0;
		
		for(;i<lastSegment.length-1;i++)
		{
			faces.addAll(this.createPlane(lastSegment[i], lastSegment[i+1], newSegment[i], newSegment[i+1]));
		}
		
		return faces;
	}
	
	protected List<Vec3> createPlane(Vec3 bottomLeft, Vec3 bottomRight, Vec3 topLeft, Vec3 topRight)
	{
		if(topRight.z<bottomRight.z || topLeft.z<bottomLeft.z)
		{
			logger.debug("specialCase!"); //TODO: improve it & implement it
		}
		
		List<Vec3> faces = new ArrayList<Vec3>();
		
		faces.add(bottomLeft);
		faces.add(bottomRight);
		faces.add(topLeft);

		faces.add(topRight);
		faces.add(topLeft);
		faces.add(bottomRight);
		
		return faces;	
	}
	
	private List<Vec3> createPlane(Vec3[] bottom, Vec3[] top)
	{
		return this.createPlane(bottom[0], bottom[1], top[0], top[1]);
	}
	
	protected Vec3[] createBranchSegment(Vec3 targetCenter, float diameter)
	{
		Vec3[] segment = new Vec3[2];
		
		float radius = diameter/2;
		float[] left = new float[]{-radius,0,0};
		float[] right = new float[]{radius,0,0};
		
		Vec3 originLeft = new Vec3(left);
		Vec3 originRight = new Vec3(right);
		
		segment[0] = calculate(originLeft);//originLeft.add(targetCenter);
		segment[1] = calculate(originRight);//originRight.add(targetCenter);
		
		return segment;
	}
	
	@Override
	public List<Vec3> getEndNodes() {
		return this.leafTargets;
	}

	@Override
	public void addNodeAt(Vec3 node)
	{
		this.addBranch(node);
	}
	
	protected void pushMemory()
	{
		this.lastSegmentStack.push(this.lastSegment);
		this.lastWasAPush = true;
	}
	
	protected void popMemory()
	{
		this.lastSegment = this.lastSegmentStack.pop();
		if (this.lastWasAPush && this.isPlant()) {
			Vec3 toAdd = new Vec3(this.target.x(), this.target.y(), this.target.z());
			this.leafTargets.add(toAdd);
		}
		this.lastWasAPush = false;
		//this.printLeafTargets();
	}
	
	public void printLeafTargets() {
		System.out.println("LEAF TARGETS:");
		for (Vec3 v : this.leafTargets) {
			System.out.println(v.toString());
		}
	}
}