/**
 * 
 */
package core.model;


import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.fhnw.util.math.Vec3;

/**
 * @author Lukas Keller
 * @version 1.0
 */
public class LSystem3DModel3DCubic extends LSystem3DModel2D
{
	private static Logger logger = LogManager.getLogger(LSystem3DModel3DCubic.class);
	
	private final double ALLOWED_ANGLE = 20; //in deg
	
	public LSystem3DModel3DCubic()
	{
		super();
	}
	
	public LSystem3DModel3DCubic(boolean isPlant)
	{
		super(isPlant);
	}
	
	protected double getAngle()
	{
		Vec3 p = new Vec3(1,1,1);
		Vec3 pRot = this.applyRotation(p);
		
		float scalar = p.dot(pRot);
		
		double angle = Math.toDegrees(Math.acos(scalar/(p.length()*pRot.length())));
		
		return angle;
	}
	
	@Override
	protected Vec3[] createBranchSegment(Vec3 targetCenter, float diameter)
	{
		Vec3[] segment = new Vec3[5];
		
		float radius = diameter/2;
		float[] frontLeft = new float[]{-radius,-radius,0};
		float[] frontRight = new float[]{radius,-radius,0};
		
		float[] backLeft = new float[]{-radius,radius,0};
		float[] backRight = new float[]{radius,radius,0};
		
		double angle = this.getAngle();
		
		Vec3 originFrontLeft = new Vec3(frontLeft);
		Vec3 originFrontRight = new Vec3(frontRight);
		
		Vec3 originBackLeft = new Vec3(backLeft);
		Vec3 originBackRight = new Vec3(backRight);
		
		if(angle>-ALLOWED_ANGLE && angle<ALLOWED_ANGLE)
		{
			originFrontLeft = this.applyRotation(originFrontLeft);
			originFrontRight = this.applyRotation(originFrontRight);
			
			originBackLeft = this.applyRotation(originBackLeft);
			originBackRight = this.applyRotation(originBackRight);
		}
		else
		{
			
		}
		
		segment[0] = originFrontLeft.add(targetCenter);
		segment[1] = originFrontRight.add(targetCenter);
		segment[2] = originBackRight.add(targetCenter);
		segment[3] = originBackLeft.add(targetCenter);
		segment[4] = segment[0];
		
		return segment;
	}
	
	protected void popMemory()
	{
		addEndFace();
		super.popMemory();
	}
	
	protected void addEndFace()
	{
		List<Vec3> endFace = this.createPlane(this.lastSegment[0], this.lastSegment[1], this.lastSegment[3], this.lastSegment[2]);
		this.vertices.addAll(endFace);
	}
}