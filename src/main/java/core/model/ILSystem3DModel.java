package core.model;

import java.util.List;

import ch.fhnw.ether.scene.mesh.IMesh;
import ch.fhnw.util.math.Vec3;

/**
 * @author Lukas Keller, Patric Sigrist
 * @version 1.2
 */
public interface ILSystem3DModel
{
	public IMesh createMesh();
	
	public void addNodeAt(Vec3 vertex);
	public void push();
	public void pop();
	public void translate(Vec3 vector);
	public void rotate(float angle, Vec3 axis);
	public void rotate(float angle);
	public void rotateTurtle(float angle, Vec3 axis);
	public void multiplyDrawLength(float factor);
	
	public int getNumberOfVertices();
	public List<Vec3> getEndNodes();
	
	public boolean isPlant();
}
