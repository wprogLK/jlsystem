/**
 * 
 */
package core.model;

import java.util.List;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.fhnw.util.math.Vec3;

/**
 * @author Lukas Keller
 * @version 2.2
 */
public class LSystem3DModel extends AbstractLSystem3DModel //TODO: Rename it to "Lsystem3DModel1D"
{
	private static Logger logger = LogManager.getLogger(LSystem3DModel.class);
	
	private Vec3 lastVertex;
	private Stack<Vec3> lastVertexStack = new Stack<Vec3>();
	
	public LSystem3DModel()
	{
		super();
		this.addOrigin();
	}
	
	public LSystem3DModel(boolean isPlant)
	{
		super(isPlant);
		this.addOrigin();
	}
	
	@Override
	public void addNodeAt(Vec3 node)
	{
		this.addVertex(node);
	}
	
	private void addVertex(Vec3 vertex)
	{
		if(this.vertices.size()>1)
		{
			this.vertices.add(this.lastVertex);
		}
		
		Vec3 target = this.calculate(vertex);
		this.vertices.add(target);
		this.lastVertex = target;
	}
	
	protected final void pushMemory()
	{
		this.lastVertexStack.push(this.lastVertex);
	}
	
	protected final void popMemory()
	{
		this.lastVertex = this.lastVertexStack.pop();
	}

	
	
}