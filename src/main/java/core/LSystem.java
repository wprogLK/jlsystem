package core;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.parser.ParseException;

import core.cache.LSystemCache;
import core.command.ICommand;
import core.evolution.Evolution;
import core.generators.CodingCommandGenerator;
import core.generators.CodingGenerator;
import core.generators.IGenerator;
import core.generators.SimpleGenerator;
import core.interpreters.CodingCommandInterpreter;
import core.interpreters.CodingInterpreter;
import core.interpreters.IInterpreter;
import core.interpreters.SimpleInterpreter;

/**
 * @author Lukas Keller
 * @version 1.4
 */
public class LSystem<T>
{
	private static Logger logger = LogManager.getLogger(LSystem.class);
	
	private final IInterpreter<T> INTERPRETER;
	private final IGenerator<T> GENERATOR;

	private Evolution[] evolutions;
	
	private LSystem(IInterpreter<T> interpreter, IGenerator<T> generator) throws Exception
	{
		this.INTERPRETER = interpreter;
		this.GENERATOR = generator;
		
		T[] rawEvolutions = this.GENERATOR.generate();
		this.evolutions = this.INTERPRETER.createEvolutions(rawEvolutions);
	}
	
	/**
	 * Creates a new instance of a simple L-System without endcoding and decoding system
	 * @param filePath
	 * @param name the name of the L-System
	 * @param maxEvolutions
	 * @return 
	 * @throws Exception 
	 * @throws IOException
	 * @throws ParseException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 */
	public static LSystem<String> createSimpleSystem(String filePath, String name, int maxEvolutions) throws Exception
	{
		LSystem<String> system = new LSystem<String>(new SimpleInterpreter(filePath),new SimpleGenerator(filePath, maxEvolutions+1)); //FIX: +1 because of evolution number 0
		
		return system;
	}
	
	/**
	 * Creates a new instance of a coding L-System
	 * @param filePath
	 * @param name the name of the L-System
	 * @param maxEvolutions
	 * @return 
	 * @throws Exception 
	 * @throws IOException
	 * @throws ParseException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 */
	public static LSystem<String> createCodingSystem(String filePath, String name, int maxEvolutions) throws Exception
	{
		LSystem<String> system = new LSystem<String>(new CodingInterpreter(filePath),new CodingGenerator(filePath, maxEvolutions+1)); //FIX: +1 because of evolution number 0
		
		return system;
	}
	
	/**
	 * Creates a new instance of a coding L-System
	 * @param filePath
	 * @param name the name of the L-System
	 * @param maxEvolutions
	 * @return 
	 * @throws Exception 
	 * @throws IOException
	 * @throws ParseException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 */
	public static LSystem<List<ICommand>> createCommandCodingSystem(String filePath, String name, int maxEvolutions) throws Exception
	{
		LSystemCache.getCache(filePath);
		LSystem<List<ICommand>> system = new LSystem<List<ICommand>>(new CodingCommandInterpreter(filePath),new CodingCommandGenerator(filePath, maxEvolutions+1)); //FIX: +1 because of evolution number 0
		
		return system;
	}
	
	public Evolution getEvolution(int evolutionNumber)
	{
		assert(evolutionNumber>=0 && evolutionNumber<evolutions.length);

		return this.evolutions[evolutionNumber];
	}
	
	public void executeEvolution(int evolutionNumber)
	{
		this.getEvolution(evolutionNumber).exec();
	}
}