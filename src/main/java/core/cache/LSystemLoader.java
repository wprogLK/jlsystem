package core.cache;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import core.command.CommandFactory;
import core.interpreters.SimpleInterpreter;

/**
 * @author Lukas Keller
 * @version 1.0
 */
public class LSystemLoader 
{
	private static Logger logger = LogManager.getLogger(LSystemLoader.class);
	
	private JSONObject jsonObject;
	
	private String name;
	private JSONObject commands;
	private JSONObject rules;
	
	private final String FILE_PATH;
	private final static String PATH ="src/main/resources/examples/";
	private final static String BASIC_FILE_PATH = "Basic.json";
	
	private final FactoryInterpreter interpreter;
	
	private final String DELIMITER = ";";
	private final String VARIABLE_NAME ="name";
	private final String VARIABLE_ARGUMENTS ="arguments";
	private final String VARIALBLE_VARIABLE_NAMES="varNames";
	
	public LSystemLoader()
	{
		this(PATH+BASIC_FILE_PATH);
	}
	
	public LSystemLoader(String filePath)
	{
		this.FILE_PATH = filePath;
		this.interpreter = new FactoryInterpreter();
		this.init();
	}
	
	private void init()
	{
		try
		{
			FileReader reader = new FileReader(this.FILE_PATH);
			
			JSONParser jsonParser = new JSONParser();
			
			this.jsonObject = (JSONObject) jsonParser.parse(reader);
			this.commands = (JSONObject) jsonObject.get("commands");
			this.rules = (JSONObject) jsonObject.get("rules");
			
			this.name = (String) jsonObject.get("name");
		} 
		catch (IOException | ParseException e)
		{
			logger.error(e.getMessage(),e);
			System.exit(1);
		}
	}
	
	public CommandFactory loadCommandFactory(String symbol)
	{
		return this.createFactory(symbol);
	}
	
	public List<CommandFactory> loadRule(String symbol)
	{
		String rule = this.loadStringRule(symbol);
		return this.interpreter.interpret(rule);
	}
	
	private String loadStringRule(String symbol)
	{
		if(this.rules.containsKey(symbol))
		{
			String rule =  (String) this.rules.get(symbol);
//			logger.info("Symbol " + symbol +" has a proper rule: " + rule);
			return rule;
		}
		else
		{
//			logger.info("Symbol " + symbol +" has itself as rule");
			return symbol; //symbol has itself as rule.
		}
		
	}
	
	private CommandFactory createFactory(String symbol)
	{
		String commandName ="";
		
		try
		{
			JSONObject parameterizedCommand =  (JSONObject) commands.get(symbol);
			
			commandName = (String) parameterizedCommand.get(VARIABLE_NAME);
			String arguments = (String) parameterizedCommand.get(VARIABLE_ARGUMENTS);
			String variableNames = (String) parameterizedCommand.get(VARIALBLE_VARIABLE_NAMES);
			
			String[] singleArguments = arguments.split(DELIMITER);
			String[] singleVariableNames = variableNames.split(DELIMITER);
			
			return new CommandFactory(symbol, commandName, singleArguments, singleVariableNames);
		}
		catch(ClassCastException e)
		{
			//it's just a basic command and not a parameterized JSON command...
			commandName = (String) commands.get(symbol);
			return new CommandFactory(symbol, commandName);
		}
	}
	class FactoryInterpreter
	{
		/**
		 * 
		 * @param rule
		 * @return
		 * @see SimpleInterpreter#interpret(String)
		 */
		public List<CommandFactory> interpret(String rule)
		{
			List<CommandFactory> factories = new ArrayList<CommandFactory>();
			
			List<String> separateSymbols = SimpleInterpreter.separateCommands(rule, commands);
			
			for(String symbol:separateSymbols)
			{
				factories.add(createFactory(symbol));
			}
			
			return factories;
		}
	}
}
