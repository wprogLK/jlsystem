/**
 * 
 */
package core.cache;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Lukas Keller
 * @version 1.0
 */
public abstract class AbstractCache<K,V>
{
	private static Logger logger = LogManager.getLogger(AbstractCache.class);
	
	private HashMap<K,V> cache;
	protected LSystemLoader loader;
	
	/**
	 * 
	 */
	public AbstractCache(LSystemLoader loader) 
	{
		this.loader = loader;
		this.cache = new HashMap<K, V>();
	}
	
	public V get(K key)
	{
		V value = null;
		
		if(this.cache.containsKey(key))
		{
			value = this.cache.get(key);
		}
		else
		{
			value =  load(key);
			this.cache.put(key, value);
		}
		
		return value;
	}
	
	protected abstract V load(K key);

}
