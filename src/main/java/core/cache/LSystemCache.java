/**
 * 
 */
package core.cache;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.command.CommandFactory;

/**
 * @author Lukas Keller
 * @version 1.1
 */
public class LSystemCache 
{
	private static Logger logger = LogManager.getLogger(LSystemCache.class);
	
	private static LSystemCache cache;
	
	private RuleCache ruleCache;
	private CommandCache commandCache;

	private LSystemCache(String filePath)
	{
		LSystemLoader loader = new LSystemLoader(filePath);
		
		this.ruleCache = new RuleCache(loader);
		this.commandCache = new CommandCache(loader);
	}
	
	public static final LSystemCache getCache(String filePath)
	{
		if(cache==null)
		{
			cache = new LSystemCache(filePath);
		}
		
		return cache;
	}
	
	public static final LSystemCache getCache() throws Exception
	{
		if(cache==null)
		{
			throw new Exception("Cache was not initalized!");
		}
		
		return cache;
	}
	
	public CommandFactory getCommandFactory(String symbol)
	{
		return this.commandCache.get(symbol);
	}
	
	public List<CommandFactory> getRule(String symbol)
	{
		return this.ruleCache.get(symbol);
	}
}

/**
 * @author Lukas Keller
 * @version 1.0
 */
class RuleCache extends AbstractCache<String, List<CommandFactory>>
{

	public RuleCache(LSystemLoader loader)
	{
		super(loader);
	}

	@Override
	protected List<CommandFactory> load(String symbol) 
	{
		return loader.loadRule(symbol);
	}
}

/**
 * @author Lukas Keller
 * @version 1.0
 */
class CommandCache extends AbstractCache<String, CommandFactory>
{

	public CommandCache(LSystemLoader loader)
	{
		super(loader);
	}

	@Override
	protected CommandFactory load(String symbol) 
	{
		return loader.loadCommandFactory(symbol);
	}
}
