package core.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.fhnw.util.math.Vec3;
import core.model.ILSystem3DModel;

/**
 * @author Lukas Keller, Patric Sigrist
 * @version 1.6
 */
public class DrawForwardCommand extends AbstractCommand implements ICommand
{
	private static Logger logger = LogManager.getLogger(DrawForwardCommand.class);
	
	private final Vec3 VERTEX;
	private final Vec3 TRANSLATE;
	
	private final float LENGTH;
//	private static final float DEFAULT_LENGHT=1.0f;
	private static final float DEFAULT_LENGHT=0.5f;
	
	public DrawForwardCommand() 
	{
		this(DrawForwardCommand.DEFAULT_LENGHT);
	}
	
	public DrawForwardCommand(float length)
	{
		this.VERTEX = new Vec3(0, 0, 0);
		this.LENGTH = length;
		this.TRANSLATE = new Vec3(this.LENGTH, 0, 0);
//		this.TRANSLATE = new Vec3(0, this.LENGTH, 0);
//		this.TRANSLATE = new Vec3(0, 0, this.LENGTH);
	}
	
	@Override
	public void execSingleStep(ILSystem3DModel model)
	{
		model.translate(this.TRANSLATE); //TODO: or sum up the vector an translate then in a single step
	}
	
	@Override
	public void execPost(ILSystem3DModel model)
	{
		model.addNodeAt(this.VERTEX);
	}
}