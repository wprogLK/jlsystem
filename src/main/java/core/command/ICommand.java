package core.command;

import java.util.List;

import core.model.ILSystem3DModel;

/**
 * @author Lukas Keller
 * @version 1.4
 */
public interface ICommand 
{
	public void exec(ILSystem3DModel model);
	public void setSteps(int steps);
	public void apply(List<ICommand> rules) throws Exception;
	public void setRules(List<CommandFactory> rules);
	
	public void setLiteral(String literal);
	public String getLiteral();
}
