package core.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.model.ILSystem3DModel;

/**
 * @author Patric Sigrist
 * @version 1.0
 */
public class MultiplyDrawLengthCommand extends AbstractCommand implements ICommand
{
    private static Logger logger = LogManager.getLogger(MultiplyDrawLengthCommand.class);
    
    private float factor = 1.0f;
    
    @Override
    public void exec(ILSystem3DModel model) 
    {
        if(this.factor>=1.0f){
            float totalFactor = this.factor*(float)this.getSteps();
            model.multiplyDrawLength(totalFactor);
        }
        else if(this.factor<1.0f){
            float totalFactor = this.factor/(float)this.getSteps();
            model.multiplyDrawLength(totalFactor);
        }
        else{
            System.out.println("System broken. Please buy a new computer...");
        }
        
    }
    
//    @Override
//    public void execSingleStep(ILSystem3DModel model)
//    {
//        model.multiplyDrawLength(this.factor);
//    }
    
    public void setFactor(float factor)
    {
        this.factor = this.factor*factor;
    }
}