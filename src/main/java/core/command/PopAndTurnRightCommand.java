package core.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.model.ILSystem3DModel;

/**
 * @author Lukas Keller
 * @version 1.2
 */
public class PopAndTurnRightCommand extends AbstractCommand implements ICommand
{
	private static Logger logger = LogManager.getLogger(PopAndTurnRightCommand.class);
	
	private final float angle = -45;
	
	@Override
	public void execSingleStep(ILSystem3DModel model)
	{
		model.pop();
		model.rotate(this.angle);
	}
}