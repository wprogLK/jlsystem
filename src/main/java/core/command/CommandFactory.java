/**
 * 
 */
package core.command;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Lukas Keller
 * @version 1.1
 */
public class CommandFactory 
{
	private final String SYMBOL;
	private final String COMMAND_NAME;
	private final String[] ARGUMENTS;
	private final String[] VARIABLE_NAMES;

	private static final String PACKAGE_NAME = "core.command.";
	private static final String SUFFIX = "Command";
	
	private static Logger logger = LogManager.getLogger(CommandFactory.class);

	public CommandFactory(String symbol, String commandName, String[] arguments, String[] variableNames)
	{
		this.SYMBOL = symbol;
		this.COMMAND_NAME = commandName;
		this.ARGUMENTS = arguments;
		this.VARIABLE_NAMES = variableNames;
	}
	
	public CommandFactory(String symbol, String commandName)
	{
		this(symbol,commandName,new String[0], new String[0]);
	}
	
	public ICommand createCommand()
	{
		try 
		{
			ICommand command = this.createNewInstance();
			command.setLiteral(this.SYMBOL);
			
			command = this.applySetters(command);
			
			return command;
		} 
		catch (InstantiationException | IllegalAccessException | ClassNotFoundException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) 
		{
			logger.error(e.getMessage(),e);
			System.exit(-1);
		}
		
		return null;
	}
	
	public ICommand createCommand(int steps)
	{
		//TODO maybe use reflection to set the steps...
		
		ICommand command = this.createCommand();
		command.setSteps(steps);
		
		return command;
	}
	
	private ICommand createNewInstance() throws InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		String className = PACKAGE_NAME+COMMAND_NAME+SUFFIX;
		
		Object instance = Class.forName(className).newInstance();
		
		return (ICommand) instance;
	}
	
	private ICommand applySetters(ICommand command) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException
	{
		//FIXME: if parameter typ is an int or Integer there is a problem. (Integer.parseInt(String)) NOT Integer.parseInteger(String)!!)
		
		for(int i=0;i<this.ARGUMENTS.length;i++)	//IMPORTANT: assumption: one varName has only one argument (they are simple getters and setters)
		{
			String argument = this.ARGUMENTS[i];
			String variableName = this.VARIABLE_NAMES[i];
			
			if(!argument.isEmpty() && !variableName.isEmpty())
			{
				Method[] methods = command.getClass().getMethods();
				String methodNameToLookup = "set" + Character.toUpperCase(variableName.charAt(0)) + variableName.substring(1);
				
				for(Method method:methods)	//TODO: Improve it...
				{
					String methodName = method.getName();
					
					if(methodName.equals(methodNameToLookup))
					{
						Class[] parameterClasses = method.getParameterTypes();
						assert(parameterClasses.length==1);
						Class inputClass = parameterClasses[0];
						
						Object parameter = null;
						Class wrapperClass = inputClass;
						
						String primitiveClassName = inputClass.getName();
						String wrapperClassName = primitiveClassName;
						
						if(primitiveClassName.startsWith("java"))
						{
							//is already a wrapper class of a primitive
							 wrapperClassName = primitiveClassName;
						}
						else
						{
							wrapperClassName = "java.lang." + Character.toUpperCase(primitiveClassName.charAt(0))+ primitiveClassName.substring(1);
						}
						
						try
						{
							wrapperClass = Class.forName(wrapperClassName);
						}
						catch(ClassNotFoundException e)
						{
							logger.info("could not found wrapperClass " + wrapperClassName);
						}
						
						if(Number.class.equals(wrapperClass.getSuperclass())) //Try a number cast
						{
							//get name of the specific number class
							String numberClassName = wrapperClass.getSimpleName();
							String parserMethodName = "parse" + numberClassName;
							
							//find the parse method
							Method parserMethod = wrapperClass.getDeclaredMethod(parserMethodName, new Class[]{String.class}); 
							parameter = parserMethod.invoke(null, argument); //first argument is null, because it's a static method 
						}
						else
						{
							parameter = inputClass.cast(argument); //Try a simple cast
						}
						
						method.invoke(command, parameter);
						break;
					}
				}
			}
		}
		
		return command;
	}
}
