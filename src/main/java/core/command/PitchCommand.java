package core.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.fhnw.util.math.Vec3;
import core.model.ILSystem3DModel;

/**
 * @author Patric Sigrist
 * @version 1.2
 */
public class PitchCommand extends AbstractCommand implements ICommand
{
    private static Logger logger = LogManager.getLogger(PitchCommand.class);
    
    private float angle = 0;
    
    @Override
    public void exec(ILSystem3DModel model) 
    {
        float totalAngle = this.angle*(float)this.getSteps();
        model.rotate(totalAngle, new Vec3(1, 0, 0));
    }
    
    public void setAngle(float angle)
    {
        this.angle = angle;
    }
}