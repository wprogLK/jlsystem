package core.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.fhnw.util.math.Vec3;
import core.model.ILSystem3DModel;
import core.model.LSystem3DModel;

/**
 * @author Patric Sigrist
 * @version 1.0
 */
public class PopAndRollRightCommand extends AbstractCommand implements ICommand
{
    private static Logger logger = LogManager.getLogger(PopAndRollRightCommand.class);
    
    private final float angle = -45;
    
    @Override
    public void execSingleStep(ILSystem3DModel model)
    {
        model.pop();
        model.rotate(this.angle, new Vec3(0,1,0));
    }
}