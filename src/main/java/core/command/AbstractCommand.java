package core.command;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.cache.LSystemCache;
import core.model.ILSystem3DModel;

/**
 * @author Lukas Keller
 * @version 1.3
 */
public abstract class AbstractCommand implements ICommand 
{
	private static Logger logger = LogManager.getLogger(AbstractCommand.class);
	
	private final int DEFAULT_STEPS = 1;
	private final int MINIMAL_STEPS = 1;
	
	private int steps;
	
	private List<CommandFactory> rules;
	private String literal;
	
	/**
	 * 
	 */
	public AbstractCommand() 
	{
		this.rules = new ArrayList<CommandFactory>();
		this.setSteps(this.DEFAULT_STEPS);
		this.literal ="";
	}

	@Override
	public void exec(ILSystem3DModel model) 
	{
		//Can be overwritten in a subclass if necessary
		
		this.execSteps(model);
	}
	
	private final void execSteps(ILSystem3DModel model)
	{
		this.execPre(model);
		
		for(int i=0;i<this.steps;i++)
		{
			this.execSingleStep(model);
		}
		
		this.execPost(model);
	}
	
	public void execSingleStep(ILSystem3DModel model)
	{
		//Can be overwritten in a subclass if necessary
	}
	
	public void execPost(ILSystem3DModel model)
	{
		//Can be overwritten in a subclass if necessary
	}
	
	public void execPre(ILSystem3DModel model)
	{
		//Can be overwritten in a subclass if necessary
	}
	
	public void setSteps(int steps)
	{
		assert(steps>=this.MINIMAL_STEPS);
		
		this.steps = steps;
	}
	
	protected int getSteps()
	{
		return this.steps;
	}
	
	public final void setRules(List<CommandFactory> rules)
	{
		this.rules = rules;
	}
	
	@Override
	public void apply(List<ICommand> rules) throws Exception
	{
		this.rules = LSystemCache.getCache().getRule(this.literal);
		
		for(CommandFactory factory:this.rules)
		{
			rules.add(factory.createCommand(this.steps));
		}
	}
	
	public void setLiteral(String literal)
	{
		if(this.literal.isEmpty())
		{
			this.literal = literal;
		}
	}
	
	public String getLiteral()
	{
		return this.literal;
	}
}