package core.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author Lukas Keller
 * @version 1.2
 */
public class NullCommand extends AbstractCommand implements ICommand
{
	private static Logger logger = LogManager.getLogger(NullCommand.class);
}
