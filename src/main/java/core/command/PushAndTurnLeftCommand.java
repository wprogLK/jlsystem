package core.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.model.ILSystem3DModel;
import core.model.LSystem3DModel;

/**
 * @author Lukas Keller
 * @version 1.1
 */
public class PushAndTurnLeftCommand extends AbstractCommand implements ICommand
{
	private static Logger logger = LogManager.getLogger(PushAndTurnLeftCommand.class);
	
	private final float angle = 45;
	
	@Override
	public void execSingleStep(ILSystem3DModel model)
	{
		model.push();
		model.rotate(this.angle);
	}
}