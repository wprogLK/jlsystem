package core.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.model.ILSystem3DModel;

/**
 * @author Lukas Keller
 * @version 1.2
 */
public class PushCommand extends AbstractCommand implements ICommand
{
	private static Logger logger = LogManager.getLogger(PushCommand.class);
	
	@Override
	public void execSingleStep(ILSystem3DModel model) 
	{
		model.push();
	}
}