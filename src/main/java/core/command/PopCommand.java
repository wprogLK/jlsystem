package core.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import core.model.ILSystem3DModel;

/**
 * @author Lukas Keller
 * @version 1.2
 */
public class PopCommand extends AbstractCommand implements ICommand
{
	private static Logger logger = LogManager.getLogger(PopCommand.class);
	
	@Override
	public void execSingleStep(ILSystem3DModel model)
	{
		model.pop();
	}
}